function getTrue(input) {
    return !input
}

it('should pass', () => {
    expect(getTrue()).toEqual(true)
});